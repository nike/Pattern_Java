package com.jeffrey.pattern.decorate;

/**
 * 功能说明：
 *
 * @author weij
 */
public interface Phone {
	public void sendMessage();
	public void callPhone();
}
